<?php

/**
 * @file
 * Functions for pages related to configuration of Cambey West integration.
 *
 * Also related administration functionality (user modification, etc.).
 */

/**
 * The administration form for the API settings.
 */
function cambey_west_admin_settings_form_api($form) {

  $form['cambey_west_user'] = array(
    '#title' => t('Cambey & West User'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('The API user for your Cambey & West subscription. Note: must be specifically requested from Cambey & West.'),
    '#default_value' => variable_get('cambey_west_user', '')
  );

  $form['cambey_west_pass'] = array(
    '#title' => t('Cambey & West Password'),
    '#type' => 'password',
    '#required' => TRUE,
    '#description' => t('The password for your Cambey & West subscription.'),
    '#default_value' => variable_get('cambey_west_pass', '')
  );

  $form['cambey_west_is_testing'] = array(
    '#title' => t('Testing?'),
    '#type' => 'checkbox',
    '#description' => t('Check this box to use the testing credentials below rather than the production credentials.'),
    '#default_value' => variable_get('cambey_west_is_testing', 0)
  );

  $form['cambey_west_is_debug'] = array(
    '#title' => t('Debug Mode?'),
    '#type' => 'checkbox',
    '#description' => t('Check this box turn on debug mode, printing things to the screen. Turn off in production.'),
    '#default_value' => variable_get('cambey_west_is_debug', 0)
  );

  $form['cambey_west_prod'] = array(
    '#title' => t('API: Production Settings'),
    '#type' => 'fieldset',
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    'cambey_west_acronym_prod' => array(
      '#title' => t('Production: Cambey & West Pub Acronym'),
      '#type' => 'textfield',
      '#size' => 3,
      '#description' => t('The API user for your Cambey & West subscription. Note: must be specifically requested from Cambey & West.'),
      '#default_value' => variable_get('cambey_west_acronym_test', '')
    ),
    'cambey_west_endpoint_prod' => array(
      '#title' => t('Production: Cambey & West API Endoint'),
      '#type' => 'textfield',
      '#description' => t('The API endpoint for Cambey & West SOAP calls.'),
      '#default_value' => variable_get('cambey_west_endpoint_prod_get', 'https://www.cambeywest.com/api/service.asmx')
    ),
  );

  $form['cambey_west_test'] = array(
    '#title' => t('API: Testing Settings'),
    '#type' => 'fieldset',
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    'cambey_west_acronym_test' => array(
      '#title' => t('Test: Cambey & West Pub Acronym'),
      '#type' => 'textfield',
      '#size' => 3,
      '#description' => t('The API user for your Cambey & West subscription. Note: must be specifically requested from Cambey & West.'),
      '#default_value' => variable_get('cambey_west_acronym_test', '')
    ),
    'cambey_west_endpoint_test' => array(
      '#title' => t('Test: Cambey & West API GET Endoint'),
      '#type' => 'textfield',
      '#description' => t('The API endpoint for Cambey & West SOAP calls.'),
      '#default_value' => variable_get('cambey_west_endpoint_test_get', 'https://www.cambeywest.com/api_test/service.asmx')
    ),
  );

  return system_settings_form($form);
}

/**
 * The administration form for general settings.
 */
function cambey_west_admin_settings_form_general($form) {

  $form['cambey_west_role'] = array(
    '#title' => t('Subscriber/Premium Role'),
    '#type' => 'select',
    '#required' => TRUE,
    '#description' => t('The role assigned to users who have access to Cambey & West.'),
    '#default_value' => variable_get('cambey_west_role', ''),
    '#options' =>
      user_roles()
  );

  $form['cambey_west_valid_editions'] = array(
    '#title' => t('Edition Codes With Access'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('List of edition codes for which access will be granted to the Subscriber Role above. separate with commas, no spaces.
    <dl>
      <dt><strong>Current (2016) C & W Edition Translations</strong></dt>
        <dd>E: Electronic</dd>
        <dd>P: Print and Electronic</dd>
    </dl>'),
    '#default_value' => variable_get('cambey_west_valid_editions', 'P,E'),
  );

  $form['cambey_west_valid_statuses'] = array(
    '#title' => t('Status Codes With Access'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('List of status codes for which access will be granted to the Subscriber Role above. separate with commas, no spaces.
      <dl>
        <dt><strong>Current (2016) C & W Status Translations</strong></dt>
          <dd>A: Active</dd>
          <dd>C: Cancel request</dd>
          <dd>F: Future start</dd>
          <dd>H: Temporary hold</dd>
          <dd>K: Computer cancel or reneged</dd>
          <dd>L: Lapsed</dd>
          <dd>N: Non-subscriber</dd>
          <dd>P: Undeliverable cancel</dd>
          <dd>R: Reserve (inactive)</dd>
          <dd>S: Suspended (nonpayment)</dd>
          <dd>U: Undeddverable</dd>
          <dd>X: Permanent delete</dd>
      </dl>'),
    '#default_value' => variable_get('cambey_west_valid_statuses', 'A,U'),
  );

  $form['cambey_west_ignore_statuses'] = array(
    '#title' => t('Rely on C & W Instead of Status Code List'),
    '#type' => 'checkbox',
    '#description' => t('Instead of relying on the above Status Codes list, grant access to Subscriber Role based whether or not C & W sets "friendcorrectiveaction". If checked, "Status Codes With Access" will be ignored.'),
    '#default_value' => variable_get('cambey_west_ignore_statuses', 0),
  );

  $form['cambey_west_premium_access_message'] = array(
    '#title' => t('Premium Access Message'),
    '#type' => 'textfield',
    '#description' => t('The message displayed in drupal_set_message (that is, as a status message) which informs the reader that they have access to premium content.'),
    '#default_value' => variable_get('cambey_west_premium_access_message', 'You have access to premium content.'),
  );

  $form['cambey_west_account_but_no_access_message'] = array(
    '#title' => t('Drupal Account But No C & W Access'),
    '#type' => 'textfield',
    '#size' => 150,
    '#description' => t('The message displayed in drupal_set_message (that is, as a status message) when a user has a Drupal account but do not have access to content. "Message Substitution Token" below will be utilized.'),
    '#default_value' => variable_get('cambey_west_account_but_no_access_message', 'You do not have access to premium content. Click here to subscribe.'),
  );

  $form['cambey_west_message_substitution'] = array(
    '#title' => t('Message Substitution Token'),
    '#type' => 'textfield',
    '#description' => t('When a user\'s subscription status is checked, Cambey & West gives back a URL and message if that subscription is lapsed or in need of payment. The URL can be linked, wrapped around any arbitrary string of text in the message. These messages are set at Cambey & West. To enable them, request that they enable "friendcorrectiveaction." By default, the string "Click here" will be linked to the URL provided by Cambey & West. Case insensitive. If not found, URL will be appended to message.'),
    '#default_value' => variable_get('cambey_west_message_substitution', 'Click here'),
  );

  $form['cambey_west_recheck_timeout'] = array(
    '#title' => t('User Permissions Expiration'),
    '#type' => 'textfield',
    '#size' => 3,
    '#field_suffix' => t('hours'),
    '#description' => t('Users may stay logged in for a long time. Because of this, a user\'s access level should be re-checked periodically. Specify time in hours after which a user\'s Cambey & West subscription level should be re-checked. Bear in mind that subscription levels are always checked at log-in'),
    '#default_value' => variable_get('cambey_west_recheck_timeout', 72)
  );

  $form['cambey_west_user_registration_text'] = array(
    '#title' => t('Text for User Pages'),
    '#type' => 'fieldset',
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    'cambey_west_user_registration_intro' => array(
      '#title' => t('User Registration Text Intro'),
      '#type' => 'text_format',
      '#description' => t('This short text will precede the user registration form.'),
      '#format' => variable_get('cambey_west_user_registration_intro', array('format' => 'filtered_html'))['format'],
      '#default_value' => variable_get('cambey_west_user_registration_intro', array('value' => ''))['value']
    ),
    'cambey_west_user_registration_subscription_option' => array(
      '#title' => t('Subscription information'),
      '#type' => 'text_format',
      '#required' => TRUE,
      '#description' => t('A description of the "Subscription" option on the user registration page that will forward users to the Cambey & West new subscription page.'),
      '#format' => variable_get('cambey_west_user_registration_intro', array('format' => 'filtered_html'))['format'],
      '#default_value' => variable_get('cambey_west_user_registration_subscription_option', array('value' => ''))['value']
    ),
    'cambey_west_user_corrective_text' => array(
      '#title' => t('User Account Corrective Text'),
      '#type' => 'text_format',
      '#description' => t('This short text will precede the user account page. It should offer a link to "/user/subscription/complete" which will re-check the user\'s access, forward them to their last-viewed piece of content, and display a message with their access level and corrective action.'),
      '#format' => variable_get('cambey_west_user_corrective_text', array('format' => 'filtered_html'))['format'],
      '#default_value' => variable_get('cambey_west_user_corrective_text', array('value' => 'Not a subscriber but want to be? Or, do you feel you should have access when you don\'t? <a href="/user/subscription/complete">Click here</a> to re-check your access against the subscription house\'s records and see a message with a link to correct your access.'))['value']
    ),
  );

  $form['cambey_west_allow_nonsubscriber_registration'] = array(
    '#title' => t('Include option for non-subscriber registration.'),
    '#type' => 'checkbox',
    '#description' => t(''),
    '#default_value' => variable_get('cambey_west_allow_nonsubscriber_registration', 0),
  );

  return system_settings_form($form);
}

/**
 * Form callback for admins to disconnect/connect drupal users.
 */
function cambey_west_admin_settings_form_user_modify($form, &$form_state) {

  $form['connect'] = array(
    '#title' => t('Connect user to Drupal ID'),
    '#type' => 'fieldset',
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => 'You must provide either email or account number',
    'cambey_west_drupal_id_connect' => array(
      '#title' => t('Drupal ID to associate'),
      '#type' => 'textfield',
      '#size' => 6,
    ),
    'cambey_west_email_connect' => array(
      '#title' => t('Email address to use for lookup.'),
      '#type' => 'textfield',
    ),
    array('#type' => 'markup', '#value' => '<div class="form-or">-- ' . t('OR') . ' --</div>'),
    'account_num_connect' => array(
      '#title' => t('Account number to use for lookup.'),
      '#type' => 'textfield',
      '#size' => 14,
    ),
  );

  // @TODO test and implement this once Cambey & West updates their code.
  $form['disconnect'] = array(
    '#title' => t('Disconnect user from Drupal ID'),
    '#type' => 'fieldset',
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => '<span style="color:red;">NOTE: This is currently not working. Waiting for Cambey & West to add this functionality to the InsertRecord_JSON function.</span>You must provide either email or account number. Drupal ID will be deleted from account.',
    'cambey_west_email_disconnect' => array(
      '#title' => t('Email address to use for lookup.'),
      '#type' => 'textfield',
    ),
    'cambey_west_account_num_disconnect' => array(
      '#title' => t('Account number to use for lookup.'),
      '#type' => 'textfield',
      '#size' => 14,
    ),

  );

  $form['lookup'] = array(
    '#title' => t('Look up users'),
    '#type' => 'fieldset',
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => 'You must provide either email, account number, or drupal_id',
    'cambey_west_drupal_id_lookup' => array(
      '#title' => t('Drupal ID to look up'),
      '#type' => 'textfield',
      '#size' => 6,
    ),
    'cambey_west_email_lookup' => array(
      '#title' => t('Email address to look up.'),
      '#type' => 'textfield',
    ),
    'cambey_west_account_lookup' => array(
      '#title' => t('Account number to look up.'),
      '#type' => 'textfield',
    ),
  );

  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Submit Values'),
    '#weight' => 99,
  );

  $form['#submit'][] = "cambey_west_user_modify_submit";
  return $form;
}

/**
 *
 */
function cambey_west_admin_settings_form_api_validate(&$form, &$form_state) {
  module_load_include('inc', 'cambey_west', 'cambey_west.api');
  $vals =& $form_state['values'];
  $env = empty($vals['cambey_west_is_testing']) ? 'prod' : 'test';
  $cw_api = new cambeyWest(
    $vals['cambey_west_user'],
    $vals['cambey_west_pass'],
    $form_state['values']["cambey_west_acronym_{$env}"],
    $form_state['values']["cambey_west_endpoint_{$env}"]
  );
  if ($cw_api->testAPIWorking()) {
    drupal_set_message('Roger. Cleared for takeoff. Connected to Cambey & West.');
  }
  else {
    form_set_error('cambey_west_user');
    form_set_error('cambey_west_pass');
    form_set_error("cambey_west_acronym_{$env}");
    form_set_error("cambey_west_endpoint_{$env}", t('There was a problem with your API credentials.'));
  }
}

/**
 *
 */
function cambey_west_user_modify_submit($form, &$form_state) {
  $vals =& $form_state['values'];
  $acct_num = isset($vals['cambey_west_account_num_connect']) ? $vals['cambey_west_account_num_connect'] : NULL;
  $drupal_id = isset($vals['cambey_west_drupal_id_connect']) ? $vals['cambey_west_drupal_id_connect'] : NULL;
  $email = isset($vals['cambey_west_email_connect']) ? $vals['cambey_west_email_connect'] : NULL;

  $acct_num_disconnect = isset($vals['cambey_west_account_num_disconnect']) ? $vals['cambey_west_account_num_disconnect'] : NULL;
  $email_disconnect = isset($vals['cambey_west_email_disconnect']) ? $vals['cambey_west_email_disconnect'] : NULL;

  $acct_num_lookup = isset($vals['cambey_west_account_lookup']) ? $vals['cambey_west_account_lookup'] : NULL;
  $drupal_id_lookup = isset($vals['cambey_west_drupal_id_lookup']) ? $vals['cambey_west_drupal_id_lookup'] : NULL;
  $email_lookup = isset($vals['cambey_west_email_lookup']) ? $vals['cambey_west_email_lookup'] : NULL;


  if ($drupal_id &&
      ($acct_num || $email)
  ) {
    cambey_west_set_user_drupal_id($drupal_id, $email, $acct_num);
  }
  if ($acct_num_disconnect || $email_disconnect) {
    cambey_west_set_user_drupal_id(NULL, $email_disconnect, $acct_num_disconnect, NULL, $unset_drupal_id = TRUE);
  }
  if ($acct_num_lookup
      || $email_lookup
      || $drupal_id_lookup) {
    $cw_api = cambey_west_get_api_interface();
    if ($acct_num_lookup) {
      $response = $cw_api->getSubscriberByAccountNum($acct_num_lookup);
      cambey_west_dpm($response, "Account #{$drupal_id_lookup}");
    }
    if ($email_lookup) {
      $response = $cw_api->getSubscriberByEmail($email_lookup);
      cambey_west_dpm($response, "Email: {$email_lookup}");
    }
    if ($drupal_id_lookup) {
      $response = $cw_api->getSubscriberByDrupalId($drupal_id_lookup);
      cambey_west_dpm($response, "Drupal ID: {$drupal_id_lookup}");
    }
  }
}

function cambey_west_dpm($response, $message = '') {
  if (!$response) {
    $response = (string)$response;
  }
  else {
    $response = xml2array($response);
  }
  if (function_exists('dpm')) {
    dpm($response, $message);
  }
  else {
    print_r($message);
    print_r(var_dump($response));
  }
}

/**
 * function xml2array
 *
 * This function is part of the PHP manual.
 *
 * The PHP manual text and comments are covered by the Creative Commons
 * Attribution 3.0 License, copyright (c) the PHP Documentation Group
 *
 * @author  k dot antczak at livedata dot pl
 * @date    2011-04-22 06:08 UTC
 * @link    http://www.php.net/manual/en/ref.simplexml.php#103617
 * @license http://www.php.net/license/index.php#doc-lic
 * @license http://creativecommons.org/licenses/by/3.0/
 * @license CC-BY-3.0 <http://spdx.org/licenses/CC-BY-3.0>
 */
function xml2array ( $xmlObject, $out = array () )
{
  foreach ( (array) $xmlObject as $index => $node )
    $out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;

  return $out;
}