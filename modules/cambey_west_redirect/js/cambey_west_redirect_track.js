/**
 * @file
 */

(function ($, Drupal, window, document) {
  'use strict';
  Drupal.behaviors.cambey_west_redirect_track = {
    attach: function (context, settings) {
      var current_path = Drupal.settings.cambey_west_redirect.current_path;
      if (current_path.indexOf('/user') != 0
          || current_path.indexOf('/edit') != -1) { // Exclude user paths unless user edit.
        $.cookie('cambey_west_redirect', current_path, { expires: 1, path: '/', domain: document.domain });
      }
    }
  };

})(jQuery, Drupal, this, this.document);
