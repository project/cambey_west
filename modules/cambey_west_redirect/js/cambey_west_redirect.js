/**
 * @file
 */

(function ($, Drupal, window, document) {
  'use strict';
  Drupal.behaviors.cambey_west_redirect = {
    attach: function (context, settings) {
      $('.messages').removeClass('messages--error').addClass('messages--status');
      var stored_last_path_visited = $.cookie('cambey_west_redirect');
      var redirect_message = 'You are being redirected to ';
      if (stored_last_path_visited && stored_last_path_visited != '/') {
        $('.messages').html(redirect_message + '<a href="' + stored_last_path_visited + '">' + stored_last_path_visited + '</a>.');
        window.location.href = stored_last_path_visited; // redirect.
      }
      else {
        window.location.href = "/";
        $('.messages').html(redirect_message + '<a href="/">the homepage.</a>');
      }
    }
  };
})(jQuery, Drupal, this, this.document);
