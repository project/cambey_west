Sponsored by CoLab Cooperative.

Cambey & West is a publishing fulfillment house that offers print-based magazine fulfillment services along with an API that validates online user credentials. This module grants a user access to content based on their subscription status.

In addition, we offer subscription links in status messages based on whether a subscriber is lapsed or a payment is due, pulling in messages and links from C & W.

This module was designed to work with the content_paywall module but it is compatible with any role-based set of drupal configurations. That is to say, this module doesn't actually restrict content access. Instead, Content Paywall module truncates content based on user role that this module sets. So this module truncates sets the appopriate access level and then Content Paywall does the work of restricting the content access.

Performance has been a consideration throughout the development of this module.

Installation steps
Download module. Enable cambey_west, cambey_west_redirect, and cambey_west_user_reg:
`drush en cambey_west, cambey_west_redirect, cambey_west_user_reg`
Set up Cambey & West
Cambey & West must be configured to redirect to http://mysite.com/user/subscription/complete after all transactions.
Set up API credentials here: /admin/config/cambey_west/api
Set up /admin/config/cambey_west
Set up permissions here: /admin/people/permissions
    The following permission should be granted the subscriber role: "Re-check Own Cambey & West access"
    The following is a restricted role that should only be assigned to site admins: "Administer Cambey & West"

Notes
At registration, we offer a form that allows users to register with Cambey & West. It sends users off to Cambey & West and then links the account with the newly created subscription process.

On login, we check for Cambey & West access by Drupal ID and then assigns a role to those users. That role can be fully configured using Drupal permissions.

This will check first by Drupal ID and then, if not found, by user email. If a matching Cambey and West user is found, then role will be elevated to the configured role.

Access is re-checked on redirect from Cambey & West to homepage and user is forwarded to the last node they browsed on.

It is recommended that you run SSL on your site while running this module to keep user data (chiefly, emails and things) safe.

Currently, users can look up their subscription info by email or account number. Then, we tie their Drupal ID to this account if no Drupal ID has been attached to it. Which is somewhat trust-based.

Make sure immediate login is set with the logintoboggan module so that when users register and are redirected to Cambey & West, the account created is tied to an Drupal account. I recommend setting up a role to hold these pre-authenticated users that have not yet verified their email address.

After installing logintoboggan, you may wish to override the message that pops up after redirect from Cambey & West by pasting the following in settings.php. The non-overridden message is confusing because the user has the subscriber role and therefore access.

$conf['locale_custom_strings_en'][''] = array(
  'A validation e-mail has been sent to your e-mail address. You will need to follow the instructions in that message in order to gain full access to the site.' => 'You have created an account and have temporary access. You will need to follow the instructions in that message to ensure continued access to all site features.',
);

For developers:
$cambey_west = new cambeyWest;
cambeyWest->getSubscriberByDrupalId($id);

See the cambey_west.api.inc file for documentation of functions.