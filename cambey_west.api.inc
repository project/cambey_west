<?php

/**
 * @file
 */

/**
 *
 */
class cambeyWest {
  private $pest = NULL;
  private $api_user = NULL;
  private $api_pass = NULL;
  private $api_acronym = NULL;
  private $default_param_array = NULL;
  private $api_endpoint = NULL;
  private $debug;
  private $last_response = NULL;

  /**
   *
   */
  function __construct($api_user, $api_pass, $api_acronym, $api_endpoint, $debug = FALSE) {
    require_once 'pest/PestXML.php';
    $this->api_user = $api_user;
    $this->api_pass = $api_pass;
    $this->api_acronym = $api_acronym;
    $this->api_endpoint = $api_endpoint;
    $this->default_param_array = array(
      'pub_acronym' => $api_acronym,
      'auth_user' => $api_user,
      'auth_pass' => $api_pass,
    );
    $this->pest = new PestXML($api_endpoint);
    $debug_var = variable_get('cambey_west_is_debug');
    $this->debug = $debug || $debug_var;
    if (!$this->debug) {
      $this->pest->throw_exceptions = FALSE;
    }
  }

  /**
   * Main access helper function. Takes drupal uid and checks C & W for
   * matching account. If not found, tries email address.
   *
   * @param int $id
   *   Drupal id.
   * @param string $email
   *   Email address.
   *
   * @return Bool|array Subscription edition and status keyed by 'edition'
   *   and 'status' or FALSE on fail.
   */
  public function getSubscriptionStatusByDrupalIdOrEmail($id = NULL, $email = '') {
    if (!$response = $this->getSubscriberByDrupalId($id)) {
      if (!$response = $this->getSubscriberByEmail($email)) {
        return FALSE;
      }
    }
    if (isset($response->SubscriberData->edition)
        && isset($response->SubscriberData->curr_status)
    ) {
      $return = $this->filterResponseFields($response);
      return $return;
    }
    else {
      $this->log('Malformed user sent from Cambey & West.', $response, array(), WATCHDOG_ERROR, TRUE, $display = TRUE, $type = 'cambey_west_api');
      return FALSE;
    }
  }

  /**
   * Get subscriber by Account Number.
   *
   * @param $acct_num
   * @param bool $filter
   *   Should filtered subscriber array be returned
   *   instead of full SimpleXMLElement?
   *
   * @return array|bool|\SimpleXMLElement Subscriber record on success, FALSE on fail.
   */
  public function getSubscriberByAccountNum($acct_num, $filter = FALSE) {
    if (!is_numeric($acct_num)) {
      $this->log("Invalid Account Number. Not a number.", array(), array(), WATCHDOG_WARNING, TRUE, $display = TRUE, $type = 'cambey_west_api');
      return FALSE;
    }
    $unique_params = array('subscriber_acctno' => trim($acct_num));
    $param_array = array_merge($unique_params, $this->default_param_array);
    $response = $this->query('GetSubscriberData_ByAcctNo', $param_array, $unique_params);
    if ($response) {
      if ($filter) {
        return $this->filterResponseFields($response);
      }
      return $response;
    }
    else {
      $this->log("No record found matching account: {$unique_params}", array(), array(), WATCHDOG_NOTICE, TRUE, $display = TRUE, $type = 'cambey_west_api');
      return FALSE;
    }
  }

  /**
   * Look up C & W account information by email.
   *
   * @param string $email
   *   A valid email.
   * @param bool $filter
   *   Should filtered subscriber array be returned
   *   instead of full SimpleXMLElement?
   *
   * @return array|bool|\SimpleXMLElement Subscriber on success, FALSE on fail.
   */
  public function getSubscriberByEmail($email, $filter = FALSE) {
    $email_filtered = filter_var($email, FILTER_SANITIZE_EMAIL);
    if (!$email_filtered == $email) {
      $this->log("Email provided was not valid.", array(), array(), WATCHDOG_NOTICE, $log = FALSE, $display = TRUE, $type = 'cambey_west_api');
      return FALSE;
    }
    $unique_params = array('subscriber_email' => $email_filtered);
    $response = $this->query('GetSubscriberData_ByEmail', array_merge($unique_params, $this->default_param_array), $unique_params);
    if ($filter) {
      return $this->filterResponseFields($response);
    }
    return $response;
  }

  /**
   * @param int $id
   *   Drupal ID to get subscription information by.
   * @param bool $test
   *   Whether or not to simply return TRUE on successful
   *   communication with C & W.
   * @param bool $filter
   *   Should filtered subscriber array be returned
   *   instead of full SimpleXMLElement?
   *
   * @return array|bool|\SimpleXMLElement
   */
  public function getSubscriberByDrupalId($id = NULL, $test = FALSE, $filter = FALSE) {
    if (empty($id)) {
      return FALSE;
    }
    $unique_params = array('drupal_id' => (int) $id);
    $response = $this->query('GetSubscriberData_ByDrupal_Id', array_merge($unique_params, $this->default_param_array), $unique_params, $test);
    if ($filter) {
      return $this->filterResponseFields($response);
    }
    return $response;
  }

  /**
   * Get a query string 'k' parameter for pre-populating C & W forms based on the
   * following parameters (at least one is required).
   *
   * @param string $drupal_id
   * @param string $email
   * @param string $username
   * @param string $acct_num
   *
   * @return bool|integer False on fail, "k" string for use in query string
   *   to be appended to URL of C & W form. Data added here will be
   *   pre-populated in form if ?k=$val is added to link.
   */
  public function insertPreProcess($drupal_id = '', $email = '', $username = '', $acct_num = '') {
    if (empty($drupal_id) && empty($email) && empty($username)) {
      $this->log('No arguments sent to insertPreProcess.', array(), array(), WATCHDOG_ERROR, TRUE, $display = TRUE, $type = 'cambey_west_api');
      return FALSE;
    }
    $xml_array = array();
    if (!empty($drupal_id)) {
      $xml_array['drupal_id'] = trim($drupal_id);
    }
    if (!empty($email)) {
      $xml_array['e_mail'] = trim($email);
    }
    if (!empty($username)) {
      $xml_array['username'] = trim($username);
    }
    if (!empty($acct_num)) {
      $xml_array['cwrec_id'] = trim($acct_num);
    }
    $xml_param = array('XMLRecord' => $this->getInsertPreprocessXmlString($xml_array));
    $response = $this->query('Insert_PreProcessData', array_merge($xml_param, $this->default_param_array), $xml_param);
    if (is_numeric($response)) {
      return $response;
    }
    else {
      $this->log("Return value from Insert_PreProcessData was not an integer. Request: {$xml_param}", $response, array(), WATCHDOG_ERROR, TRUE, $display = TRUE, $type = 'cambey_west_api');
      return FALSE;
    }
  }

  /**
   * Associate an existing C & W subscriber with a Drupal ID, giving Drupal user
   * access to website.
   *
   * @param string $acct_num Account number with which to associate Drupal ID.
   * @param string $drupal_id Drupal ID to add to account Number.
   * @param string $email to set
   * @param boolean $unset_drupal_id If TRUE, Drupal ID will be specifically unset.
   *
   * @return bool|integer Integer transaction ID on success, FALSE on fail.
   */
  function insertRecord($acct_num = '', $drupal_id = '', $email = '', $unset_drupal_id = FALSE) {
    $json_params = array(
      'pub' => $this->api_acronym,
      // These need to be uncommented when John adds "the security" to the function.
    //      'auth_user' => $this->api_user,
    //      'auth_pass' => $this->api_pass,.
    );
    $unique_params = array('drupal_id' => $drupal_id);
    if ($acct_num) {
      $unique_params['acctno'] = trim($acct_num);
    }
    if ($email) {
      $unique_params['e_mail'] = trim($email);
    }
    if ($unset_drupal_id) {
      $unique_params['webpage'] = 'CLEARDRUPALID';
    }
    $param_array = array_merge($unique_params, $json_params);
    $json = array('JSON' => json_encode($param_array));
    $response = $this->query('InsertRecord_JSON', $json, $unique_params);
    if (is_numeric($response)) {
      return $response;
    }
    else {
      $this->log("Record failed to insert. Record: {$unique_params}", array(), array(), WATCHDOG_ERROR, $log = FALSE, $display = TRUE, $type = 'cambey_west_api');
      return FALSE;
    }
  }

  /**
   *
   */
  private function filterResponseFields($response) {
    if (!$response) {
      return $response;
    }
    $data = $response->SubscriberData;
    $return = array(
      'edition' => (string) $data->edition,
      'status' => (string) $data->curr_status,
      'user_info' => array(
        'drupal_id' => isset($data->drupal_id) ? (string) $data->drupal_id : '',
        'email' => isset($data->e_mail) ? (string) $data->e_mail : '',
        'username' => isset($data->username) ? (string) $data->username : '',
        'acct_num' => (string) $data->acctno
      )
    );
    if (!empty($response->SubscriberData->friendcorrectiveaction)
        && !empty($response->SubscriberData->frienderrormsg)
        && !empty($response->SubscriberData->friendhttp)) {
      $return['corrective'] = array(
        'error' => (string) $response->SubscriberData->frienderrormsg,
        'action' => (string) $response->SubscriberData->friendcorrectiveaction,
        'url' => (string) $response->SubscriberData->friendhttp,
      );
    }
    // Can be used to return more fields with `mymodule_cambey_west_user_fields_alter`.
    drupal_alter('cambey_west_user_fields', $return, $response);
    return $return;
  }

  /**
   * @return TRUE on successful communciation with C & W, FALSE on fail.
   */
  public function testAPIWorking() {
    return $this->getSubscriberByDrupalId(99999999, $test = TRUE);
  }

  /**
   * @param $xml_array Keyed array to convert to values.
   *
   * @return string XML structured for use as first parameter XMLRecord for
   *   API method Insert_PreProcessData. See API docs.
   */
  private function getInsertPreprocessXmlString($xml_array) {
    $xml_doc = new DOMDocument();
    $data_node = $xml_doc->appendChild($xml_doc->createElement("data"));
    foreach ($xml_array as $key => $val) {
      $datarow_node = $data_node->appendChild($xml_doc->createElement("datarow"));
      $field_node = $datarow_node->appendChild($xml_doc->createElement("field", $key));
      $value_node = $datarow_node->appendChild($xml_doc->createElement("value"));
      $value_node->appendChild($xml_doc->createCDATASection($val));
      $datarow_node->appendChild($field_node);
      $datarow_node->appendChild($value_node);
    }
    return $xml_doc->saveXML();
  }

  /**
   * Yoeman function that makes API calls to Cambey & West. Logs
   * errors or throws them.
   *
   * @param string $RestFunction
   *   Function to call on Cambey & West.
   * @param array $param_array
   *   Array of args for REST query.
   *
   * @return bool|\SimpleXMLElement FALSE on fail, SimpleXMLElement on success.
   */
  private function query($RestFunction, $param_array, $unique_params = array(), $test = FALSE) {
    try {
      $params = http_build_query($param_array, '', '&');
      $this->last_response = $response_raw = $this->pest->post("/{$RestFunction}", $params);
      if (empty($response_raw)) {
        $this->log('Nothing. Response was null or did not happen.', array(), array(), WATCHDOG_ERROR, TRUE);
        return FALSE;
      }
      // Response wrangling. Different responses per method.
      // Cast to string and see if int.
      $response_string = (string) $response_raw;
      if (is_numeric($response_string)) {
        // These were failing to become SimpleXMLElements.
        return (int) $response_string;
      }
      // Suppress exception b/c may not be char-encoded.
      $response = @simplexml_load_string(htmlspecialchars_decode($response_raw), 'SimpleXMLElement', LIBXML_NOCDATA);
      if (!$response) {
        // Allow exception here.
        $response = new SimpleXMLElement($response_raw, LIBXML_NOCDATA);
      }
      if (isset($response->error)) {
        $this->log((string) $response->error, array(), array(), WATCHDOG_ERROR, $watchdog_log = TRUE);
        return FALSE;
      }
      elseif ($response->result == 'False') {
        if ($test) {
          return TRUE;
        }
        $lookup_log = json_encode($unique_params);
        $this->log("Record not found for API function {$RestFunction}: {$lookup_log}", $response, array(), WATCHDOG_NOTICE, FALSE, $display = FALSE);
        return FALSE;
      }
      return $response;
    }
    catch (Exception $e) {
      $this->log('Basic communication fail with Cambey & West. Pest error thrown.' . $e->getMessage() . "\nBacktrace: " . $e->getTraceAsString(), array(), array(), WATCHDOG_CRITICAL, TRUE);
      return FALSE;
    } finally {
      if ($this->debug) {
        // $this->log(var_dump($this->pest->last_headers));
        //        $this->log(var_dump($this->pest->last_request));
        //        $this->log(var_dump($this->pest->last_response));.
      }
    }
  }

  /**
   * @param string $message
   *   String to display as message.
   * @param array $var_dump_vars
   *   Variable that is var_dumped if in debug mode.
   * @param array $message_replacement_vars
   *   Vars passed to watchdog for replacement in messsage.
   * @param int $severity
   *   Passed to watchdog(). See @watchdog for severity levels.
   * @param bool $log
   *   Whether or not to watchdog log this error.
   * @param bool $display
   *   Whether or not to drupal_set_message this error.
   * @param string $type
   *   Module type string passed to watchdog().
   */
  public function log($message, $var_dump_vars = array(), $message_replacement_vars = array(), $severity = WATCHDOG_NOTICE, $log = FALSE, $display = TRUE, $type = 'cambey_west_api') {
    $message = 'Cambey & West Drupal API says: ' . $message;
    if ($this->debug) {
      var_dump($message);
      // var_dump($var_dump_vars);
    }
    if ($display) {
      drupal_set_message($message, 'error');
    }
    if ($log) {
      watchdog($type, $message, $message_replacement_vars, $severity);
    }
  }
}
